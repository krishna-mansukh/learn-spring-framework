package com.in28minutes.spring.learnspringframework.game;

public class SuperContra implements GamingConsole {

	public void up() {
		System.out.println("Jump..");
	}

	public void down() {
		System.out.println("Go down...");
	}

	public void left() {
		System.out.println("left..");
	}

	public void right() {
		System.out.println("Right....");
	}


}

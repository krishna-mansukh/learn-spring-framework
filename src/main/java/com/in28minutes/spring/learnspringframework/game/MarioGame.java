package com.in28minutes.spring.learnspringframework.game;

import org.springframework.stereotype.Component;

@Component
public class MarioGame implements GamingConsole {

	public void up() {
		System.out.println("Mario Jump..");
	}

	public void down() {
		System.out.println("Mario ...Go down into the hole..");
	}

	public void left() {
		System.out.println("Mario.. Run back...");
	}

	public void right() {
		System.out.println("Mario ...Run forward..");
	}


}

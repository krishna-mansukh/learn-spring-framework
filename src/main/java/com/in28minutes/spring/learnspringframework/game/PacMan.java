package com.in28minutes.spring.learnspringframework.game;

public class PacMan implements GamingConsole {

	public void up() {
		System.out.println("packman up..");
	}

	public void down() {
		System.out.println("packman down");
	}

	public void left() {
		System.out.println("packman back...");
	}

	public void right() {
		System.out.println("packman forward..");
	}


}
